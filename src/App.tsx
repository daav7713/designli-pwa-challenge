import { Autocomplete, AutocompleteItem, Card, CardBody, Code, Input } from "@nextui-org/react";
import { useState, useEffect } from "react";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";

import StocksCards from "./components/StockCards"
import useStockTrades from "./hooks/useStockTrades"
import useStockSymbols from "./hooks/useStockSymbols";
import StocksGraph from "./components/StocksGraph";
import useIsOnline from "./hooks/useIsOnline";

dayjs.extend(relativeTime);

const App = () => {
  const [stockToWatch, setStockToWatch] = useState('BINANCE:BTCUSDT');
  const [priceAlert, setPriceAlert] = useState("0");
  const { isOnline, lastConnection } = useIsOnline();
  const { symbols } = useStockSymbols();
  const { stocks } = useStockTrades(stockToWatch, priceAlert);

  const requestPermission = async () => {
    try {
      await Notification.requestPermission();
    } catch (error) {
      console.error('Unable to get permission to notify.', error);
    }
  };

  useEffect(() => {
    requestPermission();
  }, []);

  return (
    <>
      <div className="md:grid md:grid-cols-2 gap-4 flex flex-col">
        <div className="col-span-2 bg-gray-950">
          <StocksCards stocks={stocks} priceAlert={priceAlert} />
        </div>
        {
          !isOnline && (
            <Code color="warning" className="w-full mt-4">
              You are offline, please check your internet connection. Last updated: {dayjs(lastConnection).fromNow()}
            </Code>
          )
        }
        <div className="w-full p-2">
          <Card>
            <CardBody>
              <Autocomplete
                defaultItems={symbols}
                label="Select Stock Symbol to watch"
                placeholder="Stock Symbols"
                className="w-full"
                inputValue={stockToWatch}
                onInputChange={(value) => setStockToWatch(value)}
              >
                {(s) => <AutocompleteItem key={s.value}>{s.label}</AutocompleteItem>}
              </Autocomplete>

              <Input value={priceAlert} onValueChange={(value) => setPriceAlert(value)} type="text" label="Price alert" className="mt-4" />
            </CardBody>
          </Card>
        </div>
        <div className="w-full p-2">
          <Card>
            <CardBody>
              <StocksGraph stocks={stocks} />
            </CardBody>
          </Card>
        </div>
      </div>
    </>
  )
}

export default App

export interface Stock {
    p: number,
    s: string,
    t: number,
    v: number,
}
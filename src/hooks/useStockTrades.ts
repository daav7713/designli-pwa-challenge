import { useEffect, useState } from "react";
import useWebSocket, { ReadyState } from "react-use-websocket";

import { Stock } from "../interfaces/stock";
import useIsOnline from "./useIsOnline";
import { messaging } from "../firebase-init";
import { sendNotification } from "../utils";

const useStockTrades = (stockToWatch: string, priceAlert: string) => {
  const websocketUrl:string = `${import.meta.env.VITE_FINNHUB_WEBSOCKET_URL}?token=${import.meta.env.VITE_FINNHUB_TOKEN}`;
  const { isOnline } = useIsOnline();
  const { sendJsonMessage, lastJsonMessage, readyState } = useWebSocket(websocketUrl, { shouldReconnect: () => true }, isOnline);
  const [cachedData, setCachedData] = useState<Stock[]>();

  const getCachedData = async (cacheName: string, url: string) => {
    if (navigator.serviceWorker.controller) {
      caches.open(cacheName).then(function(cache) {
        cache.match(url).then(function(response) {
          response?.json().then((data: Stock[]) => {
            if(data) {
              setCachedData(data);
            }
          });
        });
      });
    }
  };

  const onMessageListener = () => {
    return new Promise((resolve) => {
      messaging.onMessage((payload) => {
        resolve(payload);
      });
    });
  }
  
  useEffect(() => {
    if (readyState === ReadyState.OPEN) {
      sendJsonMessage({'type':'subscribe', 'symbol': stockToWatch});
    }

    return () => {
      sendJsonMessage({'type':'unsubscribe', 'symbol': stockToWatch});
    }
  }, [readyState, stockToWatch]);

  useEffect(() => {
    if (navigator.serviceWorker.controller && navigator.onLine) {
      navigator.serviceWorker.controller.postMessage({
        type: 'stock-trades-msg',
        data: lastJsonMessage?.data,
      });
    }
  }, [lastJsonMessage?.data]);

  useEffect(() => {
    const haveAnyStocksDroppedBelowMinimum = lastJsonMessage?.data?.some((stock: Stock) => { 
      return stock.p < parseInt(priceAlert);
    });

    if (!("Notification" in window)) {
      alert("This browser does not support system notifications");
    } else if (Notification.permission === "granted" && haveAnyStocksDroppedBelowMinimum) {
      // Browser notification
      /* try {
        new Notification(
          "Low Stock Warning",
          {
            body: `${stockToWatch} Stock price is below the minimum`,
          }
        );
      } catch (error) {
        console.error(error);
      } */

      //FCM push notifications
      try{
        const fcmtoken = localStorage.getItem('fcm-token');
        if(fcmtoken) {
          sendNotification(fcmtoken , 'Low Stock Warning',  `${stockToWatch} Stock price is below the minimum`);
        }
      } catch (error) {
        console.error(error);
      }
    }
  }, [lastJsonMessage?.data, priceAlert]);
  
  useEffect(() => {
    const effectCallback = async () => {
      await getCachedData('stock-trades-cache', 'stock-trades-websocket-data');
    };

    effectCallback();

    return () => {
      // Cleanup function (if needed)
    };
  }, [navigator.onLine]);
  
  useEffect(() => {
    const effectCallback = async () => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const unsubscribe = await onMessageListener().then((payload: any) => {
        // Handle foreground notification
        console.log('Message received. ', payload);
        // You can show a notification UI here
      });

      return unsubscribe;
    };

    effectCallback();
  }, []);

  return {
    stocks: cachedData || lastJsonMessage?.data || []
  }

}

export default useStockTrades;
import { useState, useEffect } from 'react'

const useIsOnline = () => {
  const [isOnline, setIsOnline] = useState(navigator.onLine);
  const [lastConnection, setLastConnection] = useState(0);

  useEffect(() => {
    const handleOnline = () => {
      setIsOnline(true);
      setLastConnection(0);
      removeLastConnection();
    };
    const handleOffline = () => {
      setIsOnline(false)
      const lastConnection = Date.now();
      setLastConnection(lastConnection);
      saveLastConnection(lastConnection);
    };
  
    window.addEventListener('online', handleOnline);
    window.addEventListener('offline', handleOffline);
  
    // Cleanup
    return () => {
      window.removeEventListener('online', handleOnline);
      window.removeEventListener('offline', handleOffline);
    };
  }, []);

  const saveLastConnection = (lastConnection: number) => {
    localStorage.setItem('lastConnection', lastConnection.toString());
  }

  const removeLastConnection = () => {
    localStorage.removeItem('lastConnection');
  }

  return {
    isOnline,
    lastConnection
  }
}

export default useIsOnline
import { cleanupOutdatedCaches, precacheAndRoute } from 'workbox-precaching'
import firebase from 'firebase/compat/app';
import 'firebase/compat/messaging';

firebase.initializeApp({
  apiKey: "AIzaSyC9uGxedYusGMNEvQRidXn7sDLRu79ij4A",
  authDomain: "pwa-code-challenge-bbba3.firebaseapp.com",
  projectId: "pwa-code-challenge-bbba3",
  storageBucket: "pwa-code-challenge-bbba3.appspot.com",
  messagingSenderId: "204783155694",
  appId: "1:204783155694:web:c2a3d9a678689a40f1f6ff",
  measurementId: "G-GGQFHB0FQ3"
});

const messaging = firebase.messaging();

// Background Message Handler
messaging.onBackgroundMessage((payload: firebase.messaging.MessagePayload) => {
  console.log('[service-worker.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = payload?.notification?.title || 'Low Stock Warning';
  const notificationOptions = {
    body: payload?.notification?.body || 'Stock price is below the minimum',
  };

  self.registration.showNotification(notificationTitle, notificationOptions);
});

declare let self: ServiceWorkerGlobalScope;

precacheAndRoute(self.__WB_MANIFEST);
// Clean up old cache
cleanupOutdatedCaches();


self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});

self.addEventListener('push', (event: PushEvent) => {
  const pushMessage = event.data?.json() || {};

  const title: string = pushMessage.title || 'Low Stock Warning';
  const options: NotificationOptions = {
    body: pushMessage.body || 'Stock price is below the minimum',
  };

  event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener('notificationclick', (event: NotificationEvent) => {
  event.notification.close();
});

self.addEventListener('message', event => {
  if (event.data && event.data.type === 'stock-trades-msg') {
    cacheData(event.data.data);
  }
});

const cacheData = (data: Response) => {
  caches.open('stock-trades-cache').then(function(cache) {
    const response: Response = new Response(new Blob([JSON.stringify(data)], {type: 'application/json'}));
    cache.put('stock-trades-websocket-data', response);
  });
}

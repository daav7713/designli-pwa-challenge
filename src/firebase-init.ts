import firebase from 'firebase/compat/app';
import 'firebase/compat/messaging';

firebase.initializeApp({
    apiKey: "AIzaSyC9uGxedYusGMNEvQRidXn7sDLRu79ij4A",
    authDomain: "pwa-code-challenge-bbba3.firebaseapp.com",
    projectId: "pwa-code-challenge-bbba3",
    storageBucket: "pwa-code-challenge-bbba3.appspot.com",
    messagingSenderId: "204783155694",
    appId: "1:204783155694:web:c2a3d9a678689a40f1f6ff",
    measurementId: "G-GGQFHB0FQ3"
});
  
export const messaging = firebase.messaging();

import React from 'react'
import ReactDOM from 'react-dom/client'

import {NextUIProvider} from "@nextui-org/react";
import { finnhubClient, FinnhubProvider } from 'react-finnhub'

import App from './App.tsx'
import { messaging } from "./firebase-init";
import './index.css'

const client = finnhubClient(import.meta.env.VITE_FINNHUB_TOKEN);

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('./custom-sw.js')
  .then(registration => {
    console.log('SW registered: ', registration);
    messaging.getToken({
      vapidKey: import.meta.env.VITE_VAPID_PUBLIC_KEY,
      serviceWorkerRegistration : registration 
    })
    .then((currentToken:string) => {
      localStorage.setItem('fcm-token', currentToken);
    });
  })
  .catch(registrationError => {
    console.log('SW registration failed: ', registrationError);
  });
}

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <NextUIProvider>
      <FinnhubProvider client={client}>
        <main className="dark text-foreground bg-background h-screen">
          <App />
        </main>
      </FinnhubProvider>
    </NextUIProvider>
  </React.StrictMode>,
)

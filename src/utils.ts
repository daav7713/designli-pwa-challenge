export const sendNotification = async (token: string, title: string, body: string) => {
  try {
    const response = await fetch(`${import.meta.env.VITE_BACKEND_URL}/send-notification`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ token, title, body })
    });

    if (response.ok) {
      console.log('Notification sent successfully');
    } else {
      console.error('Failed to send notification');
    }
  } catch (error) {
    console.error('Error:', error);
  }
};